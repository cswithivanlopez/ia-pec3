using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TaiChiFormation : MonoBehaviour
{
    public Vector3 offsetFromLeader;    // offset from the Tai Chi Leader
    public NavMeshAgent taiChiFollower;
    public GameObject taiChiLeader; // Reference to the Tai Chi Leader gameobject
    
    void Start()
    {
        // Get the NavMesh component 
        taiChiFollower = gameObject.GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        // Caculate new position for Tai Chi Follower based on the Tai Chi Leader's position and the stored offset
        Vector3 newPosition = taiChiLeader.transform.TransformPoint(offsetFromLeader);

        // Move the Tai Chi Follower to its new position using NavMesh Pathfinding
        taiChiFollower.destination = newPosition;
    }
}
