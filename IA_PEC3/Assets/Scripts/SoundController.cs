using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Police siren Sound Effect from <a href="https://pixabay.com/sound-effects/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=99029">Pixabay</a>
// Money Sound Effect by <a href="https://pixabay.com/users/u_byub5wd934-32145216/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=129698">u_byub5wd934</a> from <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=129698">Pixabay</a>
public class SoundController : MonoBehaviour
{
    private AudioSource audioSource; 

    // references to the different sound files
    public AudioClip musicLoop; 
    public AudioClip policeSirenSound;
    public AudioClip MoneySound;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void StartStealingSound()
    {
        // plays the money sound (the thief is stealing)
        audioSource.clip = MoneySound;
        audioSource.Play();
        StartCoroutine(MainLoopSound());
    }

    private IEnumerator MainLoopSound()
    {
        // we wait for 2 seconds and then play the music loop
        yield return new WaitForSeconds(2);
        audioSource.clip = musicLoop;
        audioSource.Play();
    }

    public void StartStealingSoundAndPoliceSiren()
    {
        // play money sound and then police siren
        audioSource.clip = MoneySound;
        audioSource.Play();
        // we delay the start of the siren to listen to the money Stealing sound
        StartCoroutine(PoliceSiren());
    }

    private IEnumerator PoliceSiren()
    {
        // we wait for 2 seconds and then play the police siren
        yield return new WaitForSeconds(2);
        audioSource.clip = policeSirenSound;
        audioSource.Play();
    }

    public void StopPoliceSiren()
    {
        audioSource.clip = musicLoop;
        audioSource.Play();
    }
}

