using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockManager : MonoBehaviour
{

    public static FlockManager FM;      
    public GameObject unitPrefab;       
    public int numUnits = 100;
    public GameObject[] allUnits;
    public Vector3 moveLimits = new Vector3(15.0f, 0.0f, 15.0f);    // we limit their moves away from the flockManager
    
    // we create sliders to set each value, so we can play with the simulation
    [Header("Flock Settings")]
    [Range(0.0f, 5.0f)] public float minSpeed;
    [Range(0.0f, 5.0f)] public float maxSpeed;
    [Range(1.0f, 10.0f)] public float neighbourDistance;    // the higher the value the more they'll group together (in a flocking formation)
    [Range(1.0f, 5.0f)] public float rotationSpeed;

    void Start()
    {
        // we instantiate all of the units
        allUnits = new GameObject[numUnits];
        for (int i = 0; i < numUnits; ++i)
        {
            // we calculate the start position for each unit...
            Vector3 pos = this.transform.position + new Vector3(Random.Range(-moveLimits.x, moveLimits.x),
                                                                Random.Range(-moveLimits.y, moveLimits.y),
                                                                Random.Range(-moveLimits.z, moveLimits.z));

            allUnits[i] = Instantiate(unitPrefab, pos, Quaternion.identity);        // ...with rotation 0
        }

        FM = this;
    }
}