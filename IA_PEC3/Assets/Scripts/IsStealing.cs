using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pada1.BBCore;           // Code attributes
using Pada1.BBCore.Framework; // ConditionBase

[Condition("MyConditions/IsCaughtStealing")]
[Help("Checks whether the thief is caught stealing. Uses the 'StealController' bool attribute 'isCaughtStealing' to know if it was caught stealing")]

public class IsCaughtStealing : ConditionBase
{

    public override bool Check()
    {
        // we return the value of isCaughtStealing from 'StealController'
        GameObject stealController = GameObject.FindGameObjectWithTag("StealController");

        return stealController.GetComponent<StealControllerScript>().isCaughtStealing;
    }

}
