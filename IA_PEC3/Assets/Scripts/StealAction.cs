using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pada1.BBCore;           // Code attributes
using Pada1.BBCore.Tasks;     // TaskStatus
using Pada1.BBCore.Framework; // BasePrimitiveAction

[Action("MyActions/StealAction")]
[Help("When stealing is taking place, it will run the Steal method from 'StealController'. This will check if the thief was caught stealing, setting it's bool attribute to true.")]

public class StealAction : BasePrimitiveAction
{ 
    public override TaskStatus OnUpdate()
    {
        // We call the Steal method from StealController
        GameObject stealController = GameObject.FindGameObjectWithTag("StealController");
        stealController.GetComponent<StealControllerScript>().Steal();
       
        return TaskStatus.COMPLETED;
    }
}
