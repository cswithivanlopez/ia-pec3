using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;

public class MLAgentScript : Agent
{
    Rigidbody rBody;
    private float previousDistanceToTarget = float.MaxValue;  
    public LayerMask obstacleLayerMask; 
    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    public Transform Target;
    public override void OnEpisodeBegin()
    {
        // Set the initial distance to the target for the start of the episode
        previousDistanceToTarget = Vector3.Distance(this.transform.position, Target.position);

        // If the Agent fell, zero its momentum and go back to starting position
        if (this.transform.localPosition.y < 0)
        {
            this.rBody.angularVelocity = Vector3.zero;
            this.rBody.velocity = Vector3.zero;
            this.transform.localPosition = new Vector3(-16, 10, -27);
        }

        // Move the target to a new spot on the NavMesh area
        Vector3 newTargetPosition;
        bool foundPosition;

        // loop until a valid position, within the NavMesh area, is found
        do
        {
            // Generate a random position on the floor
            float randomX = Random.Range(-50, 50); // taking into consideration the size of the floor
            float randomZ = Random.Range(-50, 50); // taking into consideration the size of the floor
            newTargetPosition = new Vector3(randomX, 0, randomZ);

            // Find the nearest NavMesh area point to that random position
            UnityEngine.AI.NavMeshHit hit;
            float maxDistanceFromRandomPosition = 1.0f; //maximum distance to check for a NavMesh point from the random position
            foundPosition = UnityEngine.AI.NavMesh.SamplePosition(newTargetPosition, out hit, maxDistanceFromRandomPosition, UnityEngine.AI.NavMesh.AllAreas);

            if (foundPosition)
            {
                newTargetPosition = hit.position;
            }

        } while (!foundPosition);

        // Set the target position to the new position
        Target.position = newTargetPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        // We check if the agent has collided with the target (using a tag)
        if (other.CompareTag("target"))
        {
            SetReward(1.0f);
            EndEpisode();
        }
        
        else
        {
            // Penalty for hitting an object which is not the target
            AddReward(-0.001f);
        }
        
    }

    public override void CollectObservations(VectorSensor sensor)
    {

        // Target and Agent relative positions
        Vector3 directionToTarget = Target.position - this.transform.position;
        sensor.AddObservation(directionToTarget.normalized);
        sensor.AddObservation(directionToTarget.magnitude);

        //Agent's orientation
        sensor.AddObservation(transform.forward);

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);

        // Raycasts to detect obstacles on the X-Z plane
        float rayDistance = 20f; 
        float[] rayAngles = { 0f, 45f, 90f, 135f, 180f, 225f, 270f, 315f }; // We initiliaze it with some example angles, on the X-Z plane

        foreach (float angle in rayAngles)
        {
            Vector3 rayDirection = Quaternion.Euler(0, angle, 0) * Vector3.forward;
            RaycastHit hit;
            // Raycast at the angle on the X-Z plane, if it hits something on the obstacle layer, we record the distance
            if (Physics.Raycast(transform.position + new Vector3(0, 0.5f, 0), rayDirection, out hit, rayDistance, obstacleLayerMask))
            {
                // Normalize the distance and add as observation
                sensor.AddObservation(hit.distance / rayDistance);
            }
            else
            {
                // No hit, add max distance
                sensor.AddObservation(1.0f);
            }
        }

    }

    public float forceMultiplier = 1;
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {

        // Actions, size = 2
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = actionBuffers.ContinuousActions[0];
        controlSignal.z = actionBuffers.ContinuousActions[1];
        rBody.AddForce(controlSignal * forceMultiplier);

        // Calculate the current distance to the target
        float distanceToTarget = Vector3.Distance(this.transform.position, Target.position);

        // If the distance to target has increased, penalize the agent, if it'ts getting closer Reward
        if (distanceToTarget > previousDistanceToTarget)
        {
            AddReward((distanceToTarget - previousDistanceToTarget) * (- 0.005f));
        }
        else
        {
            AddReward((previousDistanceToTarget - distanceToTarget) * (0.1f));
        }

        // Update the previous distance for the next step
        previousDistanceToTarget = distanceToTarget;

        // Fell off platform
        if (this.transform.localPosition.y < 0)
        {
            EndEpisode();
        }
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continuousActionsOut = actionsOut.ContinuousActions;
        continuousActionsOut[0] = Input.GetAxis("Horizontal");
        continuousActionsOut[1] = Input.GetAxis("Vertical");
    }
}

