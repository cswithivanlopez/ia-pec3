using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaiChiFormationManager : MonoBehaviour
{
    
    public int followersInRow = 5;      // maximum number of TaiChi followers per row.
    public float spaceBetweenFollowers = 2f;    
    public int totalNumberOfFollowers = 20;     // total number of followers in the formation
    public GameObject taiChiFollowerPrefab;     // reference to the follower prefab to instantiate the Follower objects
    // reference to the leader to assign it as a target for each instantiated follower and to calculate the follower's starting position relative to the leader
    public GameObject taiChiLeader;             

    void Start()
    {
        for (int i = 0; i < totalNumberOfFollowers; i++)
        {
            // Row and column for the current follower
            int column = i % followersInRow;
            int row = i / followersInRow;

            // Offset for each follower calculated
            float x = spaceBetweenFollowers * (column - followersInRow / 2);
            float z = -spaceBetweenFollowers * (row + 1);
            Vector3 taiChiFollowerOffset = new Vector3(x, 0, z);

            // Instantiate the new follower object {spawn} relative to the leader
            GameObject taiChiFollower = Instantiate(taiChiFollowerPrefab, taiChiLeader.transform.TransformPoint(taiChiFollowerOffset), Quaternion.identity);

            // Assign values to its TaiChiFormation script attributes
            taiChiFollower.GetComponent<TaiChiFormation>().offsetFromLeader = taiChiFollowerOffset;
            taiChiFollower.GetComponent<TaiChiFormation>().taiChiLeader = taiChiLeader;
        }
    }

}
