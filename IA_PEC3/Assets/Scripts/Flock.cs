using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour
{

    float speed;    // the unit's speed
    bool turning = false;   // turned on when a unit hits the outer boundaries where it's allowed to go

    void Start()
    {
        // we initialise the unit speed using minimum and maximum speed from the FlockManager
        speed = Random.Range(FlockManager.FM.minSpeed, FlockManager.FM.maxSpeed);
    }

    void Update()
    {
        // we use the Unity construct Bounds, like a collision box essentialy, 
        // we create a bound of the movement space allowed
        Bounds b = new Bounds(FlockManager.FM.transform.position, FlockManager.FM.moveLimits * 2.0f);

        // if the unit is not withing the allowed boundaries, we need to turn it back direction to the flockManager
        if (!b.Contains(transform.position))    
        {
            turning = true;
        }
        else
        {
            turning = false;
        }

        if (turning)    // the unit has left the allowed boundaries, so we change its direction to go back to the flockManager
        {
            // we make the unit turn around back to the center, the flockManager.
            Vector3 direction = FlockManager.FM.transform.position - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation,
                                                  Quaternion.LookRotation(direction),
                                                  FlockManager.FM.rotationSpeed * Time.deltaTime);
        }
        else // we calculate the new direction based on flocking rules: cohesion, align, separation
        {
            // before applying the 3 rules, we allow a 10% chance to change its speed so it looks more natural
            if (Random.Range(0, 100) < 10)  
            {

                speed = Random.Range(FlockManager.FM.minSpeed, FlockManager.FM.maxSpeed);
            }

            // to avoid heavy load on calculating the flocking rules ('ApplyRules()'), we only apply these 10% of the times
            // we will also see the items moving in a straight line a bit longer, giving a more natural behaviour
            if (Random.Range(0, 100) < 10)  
            {
                ApplyRules();
            }
        }

        this.transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);   // we update the unit's speed
    }

    private void ApplyRules()   // We calculate its new direction, if needed, applying the 3 flocking rules (based on cohesion, align and separation)
    {

        //game objects {all of the units in the manager}
        GameObject[] gos;   
        gos = FlockManager.FM.allUnits; 

        Vector3 vCenter = Vector3.zero; // flock vector to the center of the group
        Vector3 vAvoid = Vector3.zero;  // vector to avoid

        float gSpeed = 0.01f;   // group speed
        float nDistance;        //neighbour distance
        int groupSize = 0;      // current group size within neighbour distance. So our 'local'group of units

        foreach (GameObject go in gos)
        {

            if (go != this.gameObject)  // we don't want to compare the unit with itself
            {
                // we calculate the distance to the next unit
                nDistance = Vector3.Distance(go.transform.position, this.transform.position);
                // if the unit is withing the neigbour distance is going to influence the unit's current trayectory
                if (nDistance <= FlockManager.FM.neighbourDistance)
                {
                    // we are going to add all of the vectors of all of the units which are in our local group together
                    vCenter += go.transform.position;
                    groupSize++;

                    // if we are too close to another unit we want to avoid that unit
                    if (nDistance < 1.0f)
                    {
                        vAvoid = vAvoid + (this.transform.position - go.transform.position);
                    }

                    // we get the flock script of the current comparison unit
                    Flock anotherFlock = go.GetComponent<Flock>();
                    // we get its speed to get an average of the group speed
                    gSpeed = gSpeed + anotherFlock.speed;
                }
            }
        }

        // if we have any 'neighbours', we need to apply the rules mentioned above
        if (groupSize > 0)
        {
            // we average the vector to the center of the group and its speed
            vCenter = vCenter / groupSize; // + (FlockManager.FM.goalPos - this.transform.position);
            speed = gSpeed / groupSize;

            if (speed > FlockManager.FM.maxSpeed)
            {
                speed = FlockManager.FM.maxSpeed;
            }

            // this is the new direction we want our unit to head in
            Vector3 direction = (vCenter + vAvoid) - transform.position;
            if (direction != Vector3.zero)
            {
                // we now slowly turn the unit into the direction it needs to point at
                transform.rotation = Quaternion.Slerp(
                    transform.rotation,
                    Quaternion.LookRotation(direction),
                    FlockManager.FM.rotationSpeed * Time.deltaTime);
            }
        }
    }
}